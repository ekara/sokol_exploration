@REM @echo off
@REM setlocal
@REM cd /D "%~dp0"

@REM :: --- Unpack Arguments -------------------------------------------------------
@REM for %%a in (%*) do set "%%a=1"
@REM if not "%mingw%"=="1" set mingw=1
@REM if not "%release%"=="1" set debug=1
@REM if "%debug%"=="1"       set release=0 && echo [debug mode]
@REM if "%release%"=="1"     set debug=0 && echo [release mode]
@REM if "%mingw%"=="1"       echo [mingw compile]

@REM :: --- Compile/Link Line Definitions ------------------------------------------
@REM set mingw_common=  -I..\code\third_party\sokol -Wall -Wextra -Wpedantic -std=c99 -Wno-missing-braces
@REM set mingw_debug=   call gcc %mingw_common% -O0 -g -D_DEBUG
@REM set mingw_release= call gcc %mingw_common% -O1 -Wl,--subsystem,windows
@REM set mingw_lib=     -mwin32 -lkernel32 -luser32 -lshell32 -lgdi32 -lole32
@REM set mingw_link=    %mingw_lib%
@REM set mingw_out=     -o

@REM :: --- Choose Compile/Link Lines ----------------------------------------------
@REM if "%mingw%"=="1"   set compile_debug=%mingw_debug%
@REM if "%mingw%"=="1"   set compile_release=%mingw_release%
@REM if "%mingw%"=="1"   set compile_link=%mingw_link%
@REM if "%mingw%"=="1"   set out=%mingw_out%
@REM if "%debug%"=="1"   set compile=%compile_debug%
@REM if "%release%"=="1" set compile=%compile_release%

@REM :: --- Prep Directories -------------------------------------------------------
@REM if not exist build mkdir build

@REM :: --- Build Everything -------------------------------------------------------
@REM pushd build
@REM del . /F /Q
@REM %compile% ..\code\main.c %compile_link% %out%main.exe || exit /b 1
@REM popd


@REM :: ----------------------------------------------------------------------------
@REM set cl_common=     /I..\external\include /utf-8 /EHsc /nologo /Z7 /W3 /sdl
@REM set cl_debug=      call cl /Od %cl_common%
@REM set cl_release=    call cl /O2 /GL /favor:blend %cl_common%
@REM set cl_lib=        /libpath:..\external\lib\raylib\msvc raylib.lib kernel32.lib user32.lib shell32.lib winmm.lib gdi32.lib msvcrt.lib opengl32.lib
@REM set cl_link=       /link /INCREMENTAL:NO /NODEFAULTLIB:libcmt %cl_lib%

@echo off
setlocal
cd /D "%~dp0"
echo [debug mode]
echo [msvc compile]
if not exist build mkdir build
pushd build
del . /F /Q
@REM cl ../code/main.c /Od /nologo /DSOKOL_GLCORE /I..\code\third_party\ kernel32.lib user32.lib gdi32.lib
gcc ../code/main.c -o main -mwin32 -O0 -DSOKOL_GLCORE -I../code/third_party/sokol -lkernel32 -luser32 -lshell32 -lgdi32 -lole32
popd