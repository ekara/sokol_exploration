# sokol_exploration

Playing around with [Sokol](https://github.com/floooh/sokol) and [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear).

Useful links:

[A Tour of sokol_gfx.h](https://floooh.github.io/2017/07/29/sokol-gfx-tour.html)