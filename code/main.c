#define SOKOL_IMPL
/* this is only needed for the debug-inspection headers */
#define SOKOL_TRACE_HOOKS
/* sokol 3D-API defines are provided by build options */
#include "third_party/sokol/sokol_app.h"
#include "third_party/sokol/sokol_gfx.h"
#include "third_party/sokol/sokol_time.h"
#include "third_party/sokol/sokol_audio.h"
#include "third_party/sokol/sokol_fetch.h"
#include "third_party/sokol/sokol_log.h"
#include "third_party/sokol/sokol_glue.h"

#include <stdio.h>

sg_pass_action pass_action;

bool key_up_is_pressed = false;

void init(void) {
  sg_setup(&(sg_desc){
    .environment = sglue_environment(),
    .logger.func = slog_func,
  });
  pass_action = (sg_pass_action) {
    .colors[0] = {
      .load_action = SG_LOADACTION_CLEAR,
      .clear_value = { 1.0f, 0.0f, 0.0f, 1.0f }
    }
  };
}

void frame(void) {
  float g = pass_action.colors[0].clear_value.g + 0.01f;
  pass_action.colors[0].clear_value.g = (g > 1.0f) ? 0.0f : g;
  if (key_up_is_pressed) pass_action.colors[0].clear_value.g = 0.0f;
  sg_begin_pass(&(sg_pass){ .action = pass_action, .swapchain = sglue_swapchain() });
  sg_end_pass();
  sg_commit();
}

void cleanup(void) {
  sg_shutdown();
}

static void input(const sapp_event* ev) {
  switch (ev->type) {
    case SAPP_EVENTTYPE_KEY_DOWN:
    {
      if (ev->key_code == SAPP_KEYCODE_UP)
      {
        key_up_is_pressed = true;
      }
    } break;
    case SAPP_EVENTTYPE_KEY_UP:
    {
      if (ev->key_code == SAPP_KEYCODE_UP)
      {
        key_up_is_pressed = false;
      }
    } break;
    case SAPP_EVENTTYPE_MOUSE_DOWN:
      if (ev->mouse_button == SAPP_MOUSEBUTTON_LEFT) {
        sapp_lock_mouse(true);
      }
      break;

    case SAPP_EVENTTYPE_MOUSE_UP:
      if (ev->mouse_button == SAPP_MOUSEBUTTON_LEFT) {
        sapp_lock_mouse(false);
      }
      break;

    case SAPP_EVENTTYPE_MOUSE_MOVE:
      if (sapp_mouse_locked()) {
        printf("EVENT CALLBACK WORKING!\n");
      }
      break;

    default:
      break;
  } 
}

sapp_desc sokol_main(int argc, char* argv[]) {
  (void)argc;
  (void)argv;
  return (sapp_desc){
    .init_cb = init,
    .frame_cb = frame,
    .cleanup_cb = cleanup,
    .event_cb = input,
    // .width = 1920,
    // .height = 1080,
    .window_title = "Clear (sokol app)",
    .icon.sokol_default = true,
    .logger.func = slog_func,
  };
}